package skolem

object ComplexityChecker {
  
  def countNegations( f : Formula, depth : Int ) : Set[Int] = f match {
    case Atomic(_,_) => Set[Int]()
    case Or( f, g ) => countNegations( f, depth ) union countNegations( g, depth )
    case And( f, g ) => countNegations( f, depth ) union countNegations( g, depth )
    case To( Not(f), g ) => countNegations(f, depth) union countNegations( g, depth )
    case To( f, g ) => countNegations( f, depth+1 ) union countNegations( g, depth )
    case Not(Not(f)) => countNegations( f, depth )    
    case Not(f) => countNegations(f, depth+1)
    case Exists( _, f ) => countNegations(f,depth) + depth
    case Forall( _, f ) => countNegations(f,depth) + depth
  }
  def countArrows( f : Formula ) : Int = f match {
    case Atomic(_,_) => 0
    case Or( f, g ) => countArrows(f) + countArrows(g)
    case And( f, g ) => countArrows(f) + countArrows(g)
    case To( f, g ) => countArrows(f) + countArrows(g) + 1
    case Not( f ) => countArrows(f)
    case Exists(_, f ) => countArrows(f)
    case Forall(_, f ) => countArrows(f)        
  }
  def scanHs( t : Term ) : Set[Int] = t match {
    case FunctionalTerm( h, subterms ) if h(0)=='h' => Set[Int]( subterms.length )
    case FunctionalTerm( _, subterms ) => subterms.flatMap(scanHs).toSet
    case Variable(_) => Set()   
  }
  def scanHs( f : Formula ) : Set[Int] = f match {
    case Atomic( _, subterms ) => subterms.flatMap( scanHs ).toSet
    case Or( f, g ) => scanHs(f) union scanHs(g)
    case And( f, g ) => scanHs(f) union scanHs(g)
    case Not( f ) => scanHs(f)
    case Forall( _,f) => scanHs(f)
  }
  def skolemArities( f : Formula ) : Set[Int] = {
    import Normalizer._
    val sko = skolem( prenex( rename( unarrow( f ) ) ) )
    scanHs( sko )
  } 
  def collectFree( t : Term , bound : Set[String] ) : Set[String] = t match {
    case Variable( x ) => if( bound.contains(x) ) Set() else Set(x)
    case FunctionalTerm( f, subterms ) => subterms.flatMap( collectFree( _,bound ) ).toSet
  }
  def collectFree( f : Formula, bound : Set[String] ) : Set[String] =  f match {
    case Atomic( _, subterms ) => subterms.flatMap( collectFree( _,bound ) ).toSet
    case Or( f, g ) => collectFree(f,bound) union collectFree(g,bound)
    case And( f, g ) => collectFree(f,bound) union collectFree(g,bound)
    case To( f, g ) => collectFree(f,bound) union collectFree(g,bound)
    case Not(f) => collectFree(f,bound)
    case Forall( x, f ) => collectFree( f, bound+x )
    case Exists( x, f ) => collectFree( f, bound+x )
  }
  def collectFree( f : Formula ) : Set[String] = collectFree( f, Set[String]() )
  def ripPrefix( f : Formula, bound : Set[String] ) : (Formula, Set[String] ) = f match {
    case Forall( x, f ) => ripPrefix( f, bound+x )
    case _ => (f,bound)
  }
  def occurCheck( f : Formula ) : Boolean = {
    import Normalizer._
    val (kernel,bound) = ripPrefix( close(skolem(prenex(rename(unarrow(f))))) , Set() )
    collectFree( kernel ).size == bound.size
  }  
  def countQuantifiers( f : Formula ) : Int = f match {
    case Atomic(_,_) => 0
    case Or( f , g ) => countQuantifiers(f)+countQuantifiers(g)
    case And(f, g ) => countQuantifiers(f)+countQuantifiers(g)
    case Not(f) => countQuantifiers(f)
    case To(f,g) => countQuantifiers(f)+countQuantifiers(g)
    case Forall(_,f) => countQuantifiers(f)+1
    case Exists(_,f) => countQuantifiers(f)+1
  }
  def checkEmbedded( f : Formula, bound : List[String] ) : Boolean = f match {
    case Atomic(_,_) => true
    case Or( f, g ) => checkEmbedded( f, bound ) && checkEmbedded( g, bound )
    case And( f, g ) => checkEmbedded( f, bound ) && checkEmbedded( g, bound )
    case To( f, g ) => checkEmbedded( f, bound ) && checkEmbedded( g, bound )
    case Not( f ) => checkEmbedded( f, bound )
    case Exists( x, f ) => (!(bound contains x )) && checkEmbedded( f, x::bound) 
    case Forall( x, f ) => (!(bound contains x )) && checkEmbedded( f, x::bound) 
  }
  def allUseful( f : Formula ) : Boolean = f match {
    case Atomic(_,_) => true
    case Or( f, g ) => allUseful(f) && allUseful(g)
    case And( f, g ) => allUseful(f) && allUseful(g)
    case To( f, g ) => allUseful(f) && allUseful(g)
    case Not( f ) => allUseful(f)
    case Exists( x, f ) => (collectFree(f) contains x) && allUseful(f)
    case Forall( x, f ) => (collectFree(f) contains x) && allUseful(f)
  }
  
  def apply( f : Formula ) : Boolean = {
    collectFree(f).size == 2 &&
    occurCheck(f) &&
    countNegations( f, 0 ) == Set(0,1,2)    &&
    countArrows( f ) == 1 &&
    skolemArities( f ) == Set(0,2) &&
    countQuantifiers( f ) == 5 &&
    checkEmbedded( f, Nil ) &&
    allUseful( f )
  }
}