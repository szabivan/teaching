package skolem

object Printer {
  def printTerm( t : Term ) : String = t match {
    case Variable(x) => x
    case FunctionalTerm( c, Nil ) => c
    case FunctionalTerm( f, subterms ) => f + subterms.map( printTerm ).mkString("(", ",", ")")
  }
  
  def printTerminal( f : Formula ) : String = f match {
    case Atomic( p, Nil ) => p
    case Atomic( p, subterms ) => p + subterms.map( printTerm ).mkString( "(", ",", ")" )
    case Or( left, right ) => "(" + printTerminal(left) + " V " + printTerminal(right) + ")" 
    case And( left, right ) => "(" + printTerminal(left) + " & " + printTerminal(right) + ")" 
    case To( left, right ) => "(" + printTerminal(left) + " -> " + printTerminal(right) + ")" 
    case Not( kernel ) => "~" + printTerminal( kernel )
    case Exists( x, kernel ) => "E"+x+printTerminal(kernel)
    case Forall( x, kernel ) => "A"+x+printTerminal(kernel)
  }
  
  def makeSub( s : String ) = s.replaceAll("(\\d)", "<sub>$1</sub>")  
  def printBrowser( t : Term ) : String = t match {
    case Variable(x) => makeSub(x)
    case FunctionalTerm( c, Nil ) => makeSub(c)
    case FunctionalTerm( f, subterms ) => makeSub(f) + subterms.map( printBrowser ).mkString("(", ",", ")")
  }
  def printBrowser( f : Formula ) : String = f match {
    case Atomic( p, Nil ) => makeSub(p)
    case Atomic( p, subterms ) => makeSub(p) + subterms.map( printBrowser ).mkString( "(", ",", ")" )
    case Or( left, right ) => "(" + printBrowser(left) + " ∨ " + printBrowser(right) + ")" 
    case And( left, right ) => "(" + printBrowser(left) + " ∧ " + printBrowser(right) + ")" 
    case To( left, right ) => "(" + printBrowser(left) + " → " + printBrowser(right) + ")" 
    case Not( kernel ) => "¬" + printBrowser( kernel )
    case Exists( x, kernel ) => "∃"+makeSub(x)+printBrowser(kernel)
    case Forall( x, kernel ) => "∀"+makeSub(x)+printBrowser(kernel)    
  }
  def printLatex( f : Formula ) : String = f match {
    case Atomic( p, Nil ) => makeSub(p)
    case Atomic( p, subterms ) => makeSub(p) + subterms.map( printBrowser ).mkString( "(", ",", ")" )
    case Or( left, right ) => "(" + printLatex(left) + " \\vee " + printLatex(right) + ")" 
    case And( left, right ) => "(" + printLatex(left) + " \\wedge " + printLatex(right) + ")" 
    case To( left, right ) => "(" + printLatex(left) + " \\to " + printLatex(right) + ")" 
    case Not( kernel ) => "\\neg " + printLatex( kernel )
    case Exists( x, kernel ) => "\\exists "+makeSub(x)+"\\,"+printLatex(kernel)
    case Forall( x, kernel ) => "\\forall "+makeSub(x)+"\\,"+printLatex(kernel)    
  }

}