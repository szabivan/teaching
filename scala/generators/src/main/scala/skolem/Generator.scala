package skolem

import scala.util.Random

object Generator {
  def generateVarName = Character.valueOf( ('x' + Random.nextInt(3)).toChar ).toString() 
  // x, y, z
  def generateVariable = Variable( generateVarName )
  // c/0, f/1, g/2
  def generateTerm( depth : Int ) : Term = depth match {
    case 0 => if( Random.nextInt(5) == 0 ) FunctionalTerm("c",Nil) else generateVariable
    case _ => Random.nextInt( 5 ) match {
      case 0 => FunctionalTerm( "f", List( generateVariable ) )
      case 1 => FunctionalTerm( "c", Nil )
      case 2 => FunctionalTerm( "g", List( generateTerm(depth-1) , generateTerm(depth-1) ) )
      case _ => generateVariable
    }
  }
  
  // p/1, q/2
  def generateAtomic : Formula = Random.nextBoolean() match {
    case true => Atomic("p", List( generateTerm(1)))
    case false => Atomic("q", List( generateTerm(1), generateTerm(1)))
  }
  
  def generateFormula( depth : Int ) : Formula = depth match {
    case 0 => generateAtomic
    case _ => Random.nextInt( 7 ) match {
      case 0 => generateAtomic
      case 1 => Not( generateFormula(depth-1) )
      case 2 => Or( generateFormula(depth-1), generateFormula(depth-1) )
      case 3 => And( generateFormula(depth-1), generateFormula(depth-1) )
      case 4 => To( generateFormula(depth-1), generateFormula(depth-1) )
      case 5 => Exists( generateVarName, generateFormula(depth-1) )
      case 6 => Forall( generateVarName, generateFormula(depth-1) )
    }
  }
}