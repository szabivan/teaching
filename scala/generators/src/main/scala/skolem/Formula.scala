package skolem

trait Term
case class Variable( name : String ) extends Term
case class FunctionalTerm( symbol : String , subterms : List[Term] ) extends Term
 
trait Formula
case class Atomic( name : String, subterms : List[Term] ) extends Formula
case class Or( left : Formula, right : Formula ) extends Formula
case class And( left : Formula, right : Formula ) extends Formula
case class Not( left : Formula ) extends Formula
case class To( left : Formula, Right : Formula ) extends Formula
case class Exists( name : String, kernel : Formula ) extends Formula
case class Forall( name : String, kernel : Formula ) extends Formula