package skolem

import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import dom.document
import scala.scalajs.js.JSApp

object TheApp extends JSApp {
  
  def createTask : Formula = {
    val task = Generator.generateFormula( 5 )
    if( ComplexityChecker( task ) ) task else createTask
  }
  
  @JSExport
  def refreshTask() : Unit = {
    import Normalizer._
    val task = createTask
    val arrowfreed = unarrow( task )
    val renamed = rename( arrowfreed )
    val prenexed = prenex( renamed )
    val skolemed = skolem( prenexed )
    val closed = close( skolemed )
    
    import Printer._
    document.getElementById("latex").innerHTML = printLatex( task )
    document.getElementById("task").innerHTML = printBrowser( task )
    document.getElementById("arrowfree").innerHTML = printBrowser( arrowfreed )
    document.getElementById("renamed").innerHTML = printBrowser( renamed )
    document.getElementById("prenex").innerHTML = printBrowser( prenexed )
    document.getElementById("skolem").innerHTML = printBrowser( skolemed )
    document.getElementById("closed").innerHTML = printBrowser( closed )
  }
  
  def main() : Unit = {}
  
  def main(args: Array[String]): Unit = {
    println( createTask )
  }

}