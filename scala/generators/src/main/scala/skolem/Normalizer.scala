package skolem

//TODO : whole stuff is too wet
object Normalizer {
  
  def unarrow( f : Formula ) : Formula = f match {
    case Atomic(_,_) => f
    case Or( left, right ) => Or( unarrow(left), unarrow(right) )
    case And( left, right ) => And( unarrow(left), unarrow(right) )
    case Not( kernel ) => {
      val kernel_ = unarrow(kernel)
      kernel_ match {
        case Not( kernel__ ) => kernel__
        case _ => Not( kernel_ )
      }
    }
    case To( left, right ) => Or( unarrow(Not(left)), unarrow(right) )
    case Exists( x, kernel ) => Exists( x, unarrow(kernel) )
    case Forall( x, kernel ) => Forall( x, unarrow(kernel) )
  }
  
  
  def rename( t : Term, subst : Map[ String, String ] ) : Term = t match {
    case Variable( name ) => if( subst.contains( name ) ) Variable( subst(name) ) else  t
    case FunctionalTerm( name, subterms ) => FunctionalTerm( name, subterms map { rename( _,subst) } )
  }

  def rename( f : Formula, subst : Map[ String, String ], next : Int ) : (Formula,Int) = f match {
    case Atomic( name, subterms ) => (Atomic( name, subterms map{ rename(_,subst) }), next )
    case Not( kernel ) => { val (kernel_,next_) = rename( kernel, subst, next); ( Not(kernel_), next_ ) }
    case Or( left, right ) => {
      val( left_,next_ ) = rename( left, subst, next )
      val( right_, next__ ) = rename( right, subst, next_ )
      ( Or(left_,right_), next__ )
    }
    case And( left, right ) => {
      val( left_,next_ ) = rename( left, subst, next )
      val( right_, next__ ) = rename( right, subst, next_ )
      ( And(left_,right_), next__ )
    }
    case Exists( name, kernel ) => {
      val (kernel_, next_) = rename( kernel, subst + ( (name, name + next) ), next+1 )
      (Exists( name+next , kernel_ ) , next_ )
    }
    case Forall( name, kernel ) => {
      val (kernel_, next_) = rename( kernel, subst + ( (name, name + next) ), next+1 )
      (Forall( name+next , kernel_ ) , next_ )
    }
  }
  
  def rename( f : Formula ) : Formula = rename( f, Map(), 1 )._1
  
  def trimKernels( f : Formula ) : Formula = f match {
    case Atomic(_,_) => f
    case Or( left, right ) => Or( trimKernels(left), trimKernels(right) )
    case And( left, right ) => And( trimKernels(left), trimKernels(right) )
    case Not( kernel ) => Not( trimKernels( kernel ) )
    case Exists( x, kernel ) => trimKernels(kernel)
    case Forall( x, kernel ) => trimKernels(kernel)
  }
  def collectQuantifiers( f : Formula, flip : Boolean, result : List[ (Boolean,String) ] ) : List[ (Boolean,String) ] = 
  f match {
    case Atomic(_,_) => result
    case Not( kernel ) => collectQuantifiers( kernel, !flip, result )
    case Or( left, right ) => {
      val result_ = collectQuantifiers( left, flip, result )
      collectQuantifiers( right, flip, result_ )
    }
    case And( left, right ) => {
      val result_ = collectQuantifiers( left, flip, result )
      collectQuantifiers( right, flip, result_ )      
    }
    case Exists( x, kernel ) => collectQuantifiers( kernel, flip, (flip,x) :: result )
    case Forall( x, kernel ) => collectQuantifiers( kernel, flip, (!flip,x) :: result )
  }
  
  def appendQuantifiers( kernel : Formula, prefix : List[ (Boolean,String) ] ) : Formula = prefix match {
    case Nil => kernel
    case (false,x) :: tail => Exists( x, appendQuantifiers( kernel, tail ) )
    case (true,x) :: tail => Forall( x, appendQuantifiers( kernel, tail ) )
  }
  
  def prenex( f : Formula ) : Formula = {
    val prefix = collectQuantifiers( f, false, Nil )
    appendQuantifiers( trimKernels( f ), prefix.reverse )
  }
  
  def substitute( t : Term, subst : Map[ String, Term ] ) : Term = t match {
    case Variable( name ) => subst.getOrElse( name, t)
    case FunctionalTerm( f, subterms ) => FunctionalTerm( f, subterms map { substitute(_,subst) } )
  }
  def substitute( f : Formula, subst : Map[ String, Term ] ) : Formula = f match {
    case Atomic( p, subterms ) => Atomic( p, subterms map { substitute( _, subst ) } )
    case Not( kernel ) => Not( substitute( kernel, subst ) )
    case Or( left, right ) => Or( substitute( left, subst ) , substitute( right, subst ) )
    case And( left, right ) => And( substitute( left, subst ) , substitute( right, subst ) )    
  }

  def collectUniversals( f : Formula, prev : List[Variable] ) : Formula = prev match {
    case Nil => f
    case head :: tail => collectUniversals( Forall( head.name, f ), tail )
  }
  
  def constructSkolem( f : Formula, prev : List[Variable], subst : Map[ String, Term ], i : Int ) : Formula = f match {
    case Forall( x, kernel ) => constructSkolem( kernel, Variable(x)::prev, subst, i )
    case Exists( x, kernel ) => {
      constructSkolem( kernel, prev, subst + ( (x, FunctionalTerm( "h" + i, prev.reverse)) ), i+1 )
    }
    case _ => collectUniversals( substitute( f, subst ) , prev )
  }
  
  def skolem( f : Formula ) : Formula = constructSkolem( f, Nil, Map(), 1 )
  
  def close( t : Term, bound : List[ String ] ) : Term = t match {
    case Variable( x ) => if( bound.contains(x) ) t else FunctionalTerm( "c" + (x(0)-'u'), Nil )  //ugly hack :P
    case FunctionalTerm( f, subterms ) => FunctionalTerm( f, subterms map( close(_,bound) ) )
  }
  def close( f : Formula, bound : List[String] ) : Formula = f match {
    case Atomic( p, subterms ) => Atomic( p, subterms map( close(_,bound) ) )
    case Or( f, g ) => Or( close(f,bound),close(g,bound) )
    case And( f, g ) => And( close(f,bound),close(g,bound) )
    case Not( f ) => Not( close(f,bound) )
    case Forall( x, f ) => Forall( x, close(f, x::bound) ) 
  }
  def close( f : Formula ) : Formula = close( f, Nil )
}