package nagyzh

import scala.util.Random
import java.io.PrintWriter

object Genarator extends App{
  
  def generateOne() : String = {
    val prefix = "{\\bf\\noindent 1. Feladat, 4 pont.} Futtasd az egyesítési algoritmust a következő literálhalmazon:"
    val tasks = Vector[String](
    "\\[\\bigl\\{~p(0,f(1),c),~p(f(2),0,3),~p(4,5,5)~\\bigr\\}.\\]",
    "\\[\\bigl\\{~p(0,1,2),~p(f(3),f(2),4),~p(f(1),3,0)~\\bigr\\}.\\]",
    "\\[\\bigl\\{~p(0,1,f(2)),~p(f(3),f(2),3),~p(1,0,f(4))~\\bigr\\}.\\]"
    )
    val vars = Random.shuffle( Vector[String]( "x", "y", "z", "u", "v", "w" ) )
    
    var original = tasks( Random.nextInt( tasks.length ) )
    for( i <- 0 until vars.length ) original = original.replaceAll(i.toString(), vars(i) )
    
    prefix + original + "\n\n\\thebox{4cm}"
  }
  
  def generateTwo() : String = {
    val prefix = "{\\bf\\noindent 2. Feladat, 3 pont.} Hajtsd végre a következő helyettesítést:"
    val halves = Random.shuffle( Vector[String](
    "\\Q x~p(g(x),y)",
    "\\Q 1~\\neg q(1,g(x))"
    ))
    val original = halves.map {
      _.replaceAll("Q", if( Random.nextBoolean() ) "exists" else "forall" ).
        replaceAll("1",if(Random.nextBoolean() )"y" else "z" )
      }.mkString(
          "\\[\\Bigl((", 
          if( Random.nextBoolean() ) ")\\to(" else ")\\wedge(",
          ")\\Bigr)~\\cdot~[x/f(x,y,z)]\\]")
    prefix + original + "\n\n\\thebox{2cm}"
  }
  
  def generateThreeFour() : String = {
    val prefix1 = "{\\bf\\noindent 3. Feladat, 5 pont.} Kezdd el az alap rezolúciós algoritmust az alábbi formulán (a szükséges átalakítások után)! Két rezolvensképzésig elég eljutni."
    val prefix2 = "\n\\thebox{8cm}\\newpage\n{\\bf\\noindent 4. Feladat, 7 pont.} Mutasd meg elsőrendű rezolúcióval (a szükséges átalakítások után), hogy az alábbi formula kielégíthetetlen:"
    
    def upgrade( clauses: List[String], liftexists : Boolean ) : String = {
      // sorrend variálni
      // P/NP -> p,\neg p, Q/NQ -> q, \neg q
      // ha liftexists: vagy f(x)-ből, vagy g(y)-ból z lesz, ennek megfelelően gyártani a prefixet
      var original = Random.shuffle( clauses ).mkString( "~\\wedge~" )
      val pnp = Random.shuffle( Vector( "p", "\\\\neg p" ) )
      val qnq = Random.shuffle( Vector( "q", "\\\\neg q" ) )
      original = original.replaceAll("NP", pnp(0) ).replaceAll("P",pnp(1)).replaceAll("NQ",qnq(0)).replaceAll("Q",qnq(1))
      if( liftexists ){
        if( Random.nextBoolean() ){
          original = "\\[\\forall x\\exists z\\forall y\\Bigl(" + original.replaceAll( "f\\(x\\)", "z" ) + "\\Bigr)\\]"
        }else{
          original = "\\[\\forall y\\exists z\\forall x\\Bigl(" + original.replaceAll( "g\\(y\\)", "z" ) + "\\Bigr)\\]"          
        }
      }else{
        original = "\\[\\forall x\\forall y\\Bigl(" + original + "\\Bigr)\\]"
      }      
      original
    }
    
    val tasks : Vector[ List[String] ] = Random.shuffle( Vector(
      List( "P(x,g(y))", "(P(f(x),y)\\to Q(y,x))", "(NP(f(x),x)\\vee NQ(x,f(x)))" ),
      List( "P(f(x),y)", "(P(x,x)\\to Q(x,g(y)))", "(NP(x,g(y))\\vee NQ(x,y))"),
      List( "P(x,f(x))", "(P(f(x),y)\\to Q(x,y))","(NP(g(y),x)\\vee NQ(f(x),y))"),
      List( "P(f(x),g(y))", "(P(x,g(y))\\to Q(x,f(x)))", "(NP(x,g(f(y)))\\vee NQ(y,x))")
    ))
    val b = Random.nextBoolean()
    
    prefix1 + upgrade(tasks(0),b) + prefix2 + upgrade(tasks(1),!b) + "\\thebox{12cm}"
  }
  
  def generateFive() : String = {
    val prefix = "{\\bf\\noindent 5. Feladat, 5 pont.} Ki lehet-e fejezni a "
    val mid    = " műveletet csak a "
    val suffix = " használatával? Ha igen, hogyan, ha nem, miért nem?\n\n\\thebox{3cm}"
    val (left,right) = Random.nextInt(5) match {
      case 0 => ("$\\uparrow$","$\\to$")
      case 1 => ("$\\downarrow$","$\\to$")
      case 2 => ("$\\wedge$","$\\to$ és $\\neg$ műveletek")
      case 3 => ("$\\neg$","$\\wedge$ és $\\to$ műveletek")
      case 4 => ("$\\downarrow$", "$\\leftrightarrow$ és $\\neg$ műveletek")
    }
    prefix + left + mid + right + suffix
  }
  
  def generateSix() : String = {
    val prefix = "{\\bf\\noindent 6. Feladat, 4 pont.} Formalizáld le "
    val tasks : Vector[String] = Vector(
        "a ,,minden bogár rovar, de nem minden rovar bogár'' mondatot abban a struktúrában, melyben a $p/1$ és $q/1$ predikátumok szemantikája: $I(p)(a)$ pontosan akkor igaz, ha $a$ bogár, $I(q)(a)$ pontosan akkor igaz, ha $a$ rovar!",
        "az ,,aki fél a Medve apjától, az nem megy be a barlangba'' mondatot abban a struktúrában, melyben a $p/2$, $q/1$ predikátumok és $c/0$, $f/1$ függvényjelek szemantikája: $I(p)(a,b)$ pontosan akkor igaz, ha $a$ fél $b$-től, $I(q)(a)$ pontosan akkor igaz, ha $a$ bemegy a barlangbe, $I(c)$ a Medve, $I(f)(a)$ pedig $a$ apja!",
        "a ,,csak a Nyuszika fél önmagától'' mondatot abban a struktúrában, melyben a $p/2$ predikátum és a $c/0$ konstans szemantikája: $I(p)(a,b)$ pontosan akkor igaz, ha $a$ fél $b$-től, $I(c)$ a Nyuszika!",
        "az ,,a zöld sárkányok mind tudnak repülni, de nem csak azok'' mondatot abban a struktúrában, melyben a $p/1$, $q/1$ és $r/1$ predikátumok szemantikája: $I(p)(a)$ pontosan akkor igaz, ha $a$ sárkány, $I(q)(a)$ pontosan akkor igaz, ha $a$ zöld és $I(r)(a)$ pontosan akkor igaz, ha $a$ tud repülni!",
        "az ,,aki az összes virágot szereti, rossz ember nem lehet'' mondatot abban a struktúrában, melyben a $p/1$, $q/2$ és $r/1$ predikátumok szemantikája: $I(p)(a)$ pontosan akkor igaz, ha $a$ virág, $I(q)(a,b)$ pontosan akkor igaz, ha $a$ szereti $b$-t és $I(r)(a)$ pontosan akkor igaz, ha $a$ rossz ember!",
        "a ,,minden jóban van valami rossz'' mondatot abban a struktúrában, melyben a $p/1$ és $q/2$ predikátumok szemantikája: $I(p)(a)$ pontosan akkor igaz, ha $a$ jó (és akkor hamis, ha $a$ rossz), $I(q)(a,b)$ pedig pontosan akkor igaz, ha az $a$ objektum benne van $b$-ben!"
    )
    prefix + tasks( Random.nextInt( tasks.length ) ) + "\n\n\\thebox{3cm}"
  }
  
  def generateOneZh() : String = {
    val prefix = "\\newpage\\begin{center}Logika és informatikai alkalmazásai ZH, 2017.\\end{center}\n"+
                 "Név: \\begin{tabular}{|p{4cm}|}\\hline\\\\[1pt]\\hline\\end{tabular}\\hfil\n"+
                 "EHA: \\begin{tabular}{|p{3cm}|}\\hline\\\\[1pt]\\hline\\end{tabular}\\hfil\n"+
                 "Neptun (ha tudod): \\begin{tabular}{|p{3cm}|}\\hline\\\\[1pt]\\hline\\end{tabular}\\hfil\n\n"+
                 "\\hfil Gyakvez: Gazdag Zsolt\\begin{tabular}{|p{1pt}|}\\hline\\\\[1pt]\\hline\\end{tabular}\n"+
                 "Gelle Kitti\\begin{tabular}{|p{1pt}|}\\hline\\\\[1pt]\\hline\\end{tabular}\n"+
                 "Iván Szabolcs\\begin{tabular}{|p{1pt}|}\\hline\\\\[1pt]\\hline\\end{tabular}\n"+
                 "Németh Zoltán\\begin{tabular}{|p{1pt}|}\\hline\\\\[1pt]\\hline\\end{tabular}\\hfil\\vskip 6pt\n"
    prefix + generateOne() + "\n" + generateTwo() + "\n" + generateThreeFour() + "\n" + generateFive() + "\n" + generateSix() + "\n"

  }
  
  def generateFullLatex( n : Int ) : String = {
    val prefix =  "\\documentclass[a4paper,12pt]{article}\n" +
                  "\\usepackage[margin=1.5cm]{geometry}\n" +
                  "\\usepackage{amsmath,amssymb,hyperref}\n" +
                  "\\usepackage[utf8]{inputenc}\n" + 
                  "\\setlength{\\parskip}{0.1in}\n" +
                  "\\setlength{\\parindent}{0pt}\n" +
                  "\\tolerance 8000\n" +
                  "\\hbadness 8000\n" +
                  "\\def\\thebox#1{\\begin{tabular}{|p{\\textwidth}|}\\hline\\\\[#1]\\hline\\end{tabular}\\vskip 6pt}\n" +
                  "\\pagestyle{empty}\n" +
                  "\\begin{document}\n"
    val suffix = "\\end{document}"
    prefix + ((1 to n).map{ _=>generateOneZh() }).mkString("\n") + suffix
  }
  
  val writer = new PrintWriter( "C:/users/szabivan/git/tex/uni/logika/2017/zh2017-final.tex" )
  writer.println( generateFullLatex(285) )
  writer.close()
  
}