package week04.listOps

import week03.patternmatch.IntList
import week03.patternmatch.Nil
import week03.patternmatch.Cons

object MapFilterFold {
  
  // emeljük négyzetre a lista elemeit
  def square( list : IntList ) : IntList = list match {
    case Nil => Nil
    case a Cons tail => (a*a) :: square( list )
  }
  
  // csak a páros számokat tartsuk meg a listából
  def evensOnly( list : IntList ) : IntList = list match {
    case Nil => Nil
    case a Cons tail => if( a % 2 == 0 ) Cons( a , evensOnly( tail ) ) else evensOnly( tail )
  }
  
  // adjuk össze a lista elemeit
  def sumList( list : IntList ) : Int = list match {
    case Nil => 0
    case a Cons tail => a + sumList( tail )
  }
  
}