package week07.forComp

import scala.annotation.tailrec
import scala.collection.generic.FilterMonadic

trait MyList[+T] {
  def ::[U >: T]( head : U ) =  week07.forComp.::( head, this )
  override def toString() = Printer.printMyList( this )
  def map[U]( f : T => U ) : MyList[U]
  def flatMap[U]( f : T => MyList[U] ) : MyList[ U ]
  def ++[U >: T]( rhs : MyList[U] ) : MyList[U] 
  def :::[U >: T]( lhs : MyList[U] ) : MyList[U]
  def reverse : MyList[T]
  def filter( p : T => Boolean ) : MyList[T]  
  def foreach( p : T => Unit ) : Unit
}

case class ::[T]( head : T, tail : MyList[T] ) extends MyList[T] {
  import week07.forComp.::._
  def map[U]( f : T => U ) = f(head) :: tail.map( f )  
  def reverse = reverseAndAppend( this, MyNil )  
  //def ++[U >: T]( rhs : MyList[U] ) = head :: (tail ++ rhs)  //not so tail recursive
  def ++[U >: T]( rhs : MyList[U] ) = reverseAndAppend( reverse, rhs )
  def :::[U >: T]( lhs : MyList[U] ) = reverseAndAppend( lhs.reverse, this )
  def flatMap[U]( f : T => MyList[U] ) = f(head) ++ tail.flatMap(f) //not so tail recursive
  def filter( p : T => Boolean ) = if( p(head) ) head :: tail.filter(p) else tail.filter(p) //not so tail recursive
  def foreach( p : T => Unit ) = { p(head);tail.foreach(p) } // not so closure
}
// companion object: no parameters, not case object
object :: {
  @tailrec
  //private can be accessed from the case class after import
  private def reverseAndAppend[T]( from : MyList[T], to : MyList[T] ) : MyList[T] = from match {
    case MyNil => to
    case head :: tail => reverseAndAppend( tail, head :: to )
  }
}
case object MyNil extends MyList[Nothing] {
  def map[U]( f : Nothing => U ) = MyNil
  def flatMap[U]( f : Nothing => MyList[U] ) = MyNil
  def :::[U]( lhs : MyList[U] ) = lhs
  def ++[U]( rhs : MyList[U] ) = rhs
  def reverse = MyNil
  def filter( p : Nothing => Boolean ) = MyNil
  def foreach( p : Nothing => Unit ) = ()
}
object Printer {
  def printMyList[T]( list : MyList[T] ) : String = list match {
    case MyNil => ""
    case head :: MyNil => head.toString()
    case head :: tail => head.toString() + "," + printMyList( tail )
  }
}

object DuplicateRemover {
  def unique[T]( list : MyList[T] ) : MyList[T] = list match {
    case MyNil => MyNil
    case head :: tail => head :: unique( tail filter( head != _ ) )
  }
}


object Main extends App {
  val list = 1 :: 4 :: 2 :: 8 :: 5 :: 7 :: MyNil
  val list2 = 1 :: 2 :: 3 :: MyNil

  //one generator -- generators introduce new 'variables'
  val newlist = for( elem <- list ) yield { 2*elem }  // calls map
  
  println( newlist )
  
  //two generators
  val newlist2 = for( elem <- list ; elem2 <- list2 ) yield { elem*elem2 }  // first calls flatMap, second calls map  
  println( newlist2 )
  val newlist2b = list.flatMap( elem => list2.map( elem2 => elem*elem2 ) )
  println( newlist2b )
  
  //two generators and a filter
  val newlist3 = for( elem <- list ; elem2 <- list2 if ((elem*elem2)%2==1) ) yield{ elem * elem2 }
  println( newlist3 )
  
  //generator, filter, generator, filter. lehetne withFilter is, akkor arra defaultolna 
  val newlist4 = for( elem <- list if ( elem < 6 ) ; elem2 <- list2 if ((elem*elem2)%2==1) ) yield{ elem * elem2 }
  println( newlist4 )
  
  //nincs yield -> foreach
  for( elem <- list; elem2 <- list2 if( elem*elem2 > 10 ) ) println( elem*elem2 );
  
  println( DuplicateRemover.unique( 1 :: 4 :: 2 :: 8 :: 4 :: 2 :: 9 :: 1 :: 16 :: 0 :: MyNil ) )
}