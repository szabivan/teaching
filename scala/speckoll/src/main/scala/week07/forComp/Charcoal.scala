package week07.forComp

trait Charcoal
trait LighterFluid
trait Meat
trait Fire
trait Steak
trait Dinner

object NullGetter extends App {
  //vannak ezek a metódusok, null-or-object
  def getCharcoal : Charcoal = new Charcoal{}  //might be null
  def getLighterFluid : LighterFluid = new LighterFluid{}
  def getMeat : Meat = new Meat{}
  def lightFire( c : Charcoal, lf : LighterFluid ) : Fire = new Fire{}
  def seasonMeat( m : Meat ) : Steak = new Steak{}
  def grill( s : Steak, f : Fire ) : Dinner = new Dinner{}
  def orderPizza : Dinner = new Dinner{}
  
  def makeDinner() : Dinner = {
    //és akkor csináljunk ebédet
    val charcoal = getCharcoal
    val lighterFluid = getLighterFluid
    val meat = getMeat
  
    if( charcoal != null && lighterFluid != null ){
      val fire = lightFire( charcoal, lighterFluid )
      if( fire != null ){
        val steak = seasonMeat( meat )
        if( steak != null ){
          val dinner = grill( steak, fire )
          if( dinner != null ) return dinner //btw NEVER use return
        }
      }
    }
    orderPizza
  }  
}

/*
 * Nullázható objektumok more safe & wrapped kezelése: az Option monád!
 * Egy Option[+T] lehet..
 * ..Some[T]( v : T )
 * ..None
 * 
 * kb. a None a null.
 * Működik rajta a flatMap:
 * ..Some[T](v).flatMap[U]( f : T => Option[U] ) : Option[U] = f(v)
 * ..None.flatMap(f) = None
 * és emiatt pl. működik rajta a for comprehension is:
 * for( v <- o ) yield v -- Some(v) esetében egy (v), None esetében () lesz az eredmény (Some(v) és None, resp),
 * 
 */
object OptionGetter extends App {
  def getCharcoal : Option[Charcoal] = Some( new Charcoal{} ) //might be null
  def getLighterFluid : Option[LighterFluid] = Some( new LighterFluid{} )
  def getMeat : Option[Meat] = Some( new Meat{} )
  def lightFire( c : Charcoal, lf : LighterFluid ) : Option[Fire] = Some( new Fire{} )
  def seasonMeat( m : Meat ) : Option[ Steak ] = Some( new Steak{} )
  def grill( s : Steak, f : Fire ) : Option[ Dinner ] = Some( new Dinner{} )
  def orderPizza : Dinner = new Dinner{}
  
  def makeDinner : Dinner = {
    //csináljunk ebédet megint
    val dinnerOption = 
      for(  charcoal <- getCharcoal; //ha bármelyik ezek közül None-t ad vissza, akkor az egész is
          lighterFluid <- getLighterFluid;
          meat <- getMeat;
          fire <- lightFire( charcoal, lighterFluid );
          steak <- seasonMeat( meat );
          dinner <- grill( steak, fire )
        ) yield dinner  //különben pedig kapunk egy Some(dinner)-t
     dinnerOption.getOrElse( orderPizza )  // getOrElse : ha Some(v), akkor v, ha None, akkor orderPizza
  }
}