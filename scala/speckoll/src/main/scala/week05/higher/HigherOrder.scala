package week05.higher

import scala.annotation.tailrec

/*
 * Az előző heti taskok kicsit kulturáltabban, a beépített List[T] és metódusai használatával:  
 */
object HigherOrder {
  def square( n : Int ) = n*n 
  /*
   * map( f : T => U ) -- a lista minden elemére alkalmazza az f függvényt, új listát készít
   */
  def squareList( list : List[Int] ) = list map square
  
  def mapEquivalent[T,U]( list : List[T], f : T => U ) : List[U] = list match {
    case Nil => Nil
    case head :: tail => f(head) :: mapEquivalent( tail, f )
  }
  
  def mapEquivalentClosure[T,U]( list : List[T], f : T => U ) : List[ U ] = {
    def closure( list : List[ T ] ) : List[U] = list match {
      case Nil => Nil
      case head :: tail => f(head) :: closure( tail )
    }
    closure( list )
  }
  
  def isEven( n : Int ) = { n%2 == 0 }
  /*
   * filter( p : T => Boolean ) -- a lista azon elemeit tartja meg, amire igaz a p predikátum
   * 
   * van még:
   * filterNot( p : T => Boolean ) -- ez azokat tartja meg, amikre nem igaz a p predikátum
   * partition( p : T => Boolean ) -- ez a (filter(p), filterNot(p)) párt adja vissza 
   */
  def retainEvens( list : List[Int] ) = list filter isEven
  
  def filterEquivalent[T]( list : List[T], p : T => Boolean ) : List[T] = list match {
    case Nil => Nil
    case head :: tail => if( p(head) ) head :: filterEquivalent( tail, p ) else filterEquivalent( tail, p )
  }
  
  def sum( a : Int, b : Int ) = a + b
  /*
   * reduce[ U >: T] ( f : (U,U) => U ) -- alkalmazza az input NEMÜRES listán az f műveletet NEMDETERMINISZTIKUS sorrendben
   * ha reduce-olunk, legyen a műveletünk asszociatív
   * ha üres a lista, dob egy java.lang.UnsupportedOperationException -t
   * 
   * van még:
   * reduceLeft[ U >: T ]( f : (U,T) => U ) -- balról kezdi és göngyöli @tailrec
   * reduceRight[ U >: T ]( f : (T,U) => U ) -- jobbról kezdi és göngyöli. Ez előbb .reverse-li a listát és hív egy reduceLeft-et
   * foldLeft[ U ]( acc : U )( f : (U,T) => U ) -- mint a reduceLeft, de működik üres listán is, acc-nál kezd és nem kell, hogy U >: T legyen
   * foldRight[ U ]( acc : U )( f : (T,U) => U ) -- mint a reduceRight folddal
   */
  def sumList( list : List[ Int ] ) = list reduce sum
  def sumListEmptyAlso( list : List[ Int ] ) = list.foldLeft(0)(sum)
  
  def reduceLeftEquivalent[T, U >: T ] ( list : List[T], f : (U,U) => U ) : U = {
    @tailrec
    def accu( list : List[T], acc : U ) : U = list match {
      case Nil => acc
      case head :: tail => accu( tail, f(acc,head) )
    }
    list match {
      case Nil => throw new java.lang.UnsupportedOperationException("empty list reduce")
      case head :: tail => accu( tail, head )
    }    
  }
  
  def main(args: Array[String]): Unit = {
    val list = List( 1,4,2,8,5,7 )
    println( squareList( list ) ) //List(1, 16, 4, 25, 49)
    println( mapEquivalent( list, { x:Int => x*x } ) )
    println( mapEquivalentClosure(list, { x : Int => x*x } ))
    
    println( retainEvens( list ) )  // List(4,2,8)
    println( filterEquivalent( list, isEven ) )
    
    println( sumList( list ) ) // 27
    // println( sumList( Nil ) ) // throws java.lang.UnsupportedOperationException
    println( sumListEmptyAlso( Nil ) )  // 0
    println( sumListEmptyAlso( list ) ) // 27
    println( reduceLeftEquivalent(list, sum) ) // 27
    
    println( list reduceLeft { 2*_+_ } ) //161 -- 2*(2*(2*(2*(2*1+4)+2)+8)+5)+7
    println( list reduceRight{ 2*_+_ } ) //47  -- 2*1 + (2*4 + (2*2 + (2*8 + (2*5 + 7)))) 
    println( list.par reduce { 2*_+_ } ) //nálam épp 57: 2*(2*1 + (2*4+2))+(2*8+(2*5+7)) par: párhuzamos számításra alkalmasabb konténer lesz belőle
    //note: így a listre nem érdemes part rakni, végigmegy rajta és csinál belőle egy pararrayt, annyi idő alatt lemenne a reduce is
  }
}