package week05.myList

/*
 *  +T : kovariáns T-ben.
 *  Ha U >: T , akkor MyList[ U ] >: MyList[ T ].
 *  
 *  sealed : csak ebben a file-ban lehet extendelni közvetlenül.
 *  Ez azért jó, mert ekkor tudja teljesség-ellenőrizni a match kifejezéseket a fordító
 *  és dob warningot, ha van olyan eset, amit nem fed le a match.
 */
sealed trait MyList[ +T ] {
  /*
   * [ U >: T ] : alsó korlát a típusra. "Ha bejön egy U típusú head, ahol
   * az U az egy ősosztálya a T-nek, akkor az eredmény legyen egy MyList[U],
   * mégpedig a this mint MyList[U] (ez kovariancia miatt sikerül) és a head : U
   * ::-ja
   * 
   * a package megadásával a lentebbi case class egy példányát hozzuk létre
   */
  def ::[U >: T]( head : U ) = week05.myList.::( head, this )
}

/*
 * Nil, az üres lista, MyList[Nothing]. A kovariancia miatt minden MyList[ T ]-nek megfelelő típusú,
 * mert a Nothing minden osztály alatt van 
 */
case object Nil extends MyList[ Nothing ]
case class ::[T]( head : T , tail : MyList[T] ) extends MyList[ T ]

object MyList{
  def apply() = Nil
  def apply[T]( a1 : T ) = a1 :: Nil
  def apply[T]( a1 : T, a2 : T ) = a1 :: a2 :: Nil
  def apply[T]( a1 : T, a2 : T, a3 : T ) = a1 :: a2 :: a3 :: Nil
}

object Main extends App {
  
  /*
   * parametrikus listakiíró
   */
  def prettyPrint[T]( list : MyList[ T ] ) : String = list match {
    case Nil => ""
    /*
     * a head :: Nil azért működik match patternként, mert van egy :: nevű case class, ami két paraméteres.
     * Ez így ugyanaz, mint ::(head, Nil)
     */
    case head :: Nil => head.toString()
    /*
     * itt a lista legalább kételemű, mert az egyelemű listát az előző ág lefedte
     */
    case head :: tail => head.toString() + "," + prettyPrint( tail ) 
  }
  
  val list1 = "sanyi" :: 5 :: 3 :: Nil
  val list2 = MyList( "sanyi", 5, 3 )
  
  println( prettyPrint( list1 ) ) // sanyi,5,3
  println( prettyPrint( list2 ) ) // sanyi,5,3
}