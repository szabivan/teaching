package week05.mergesort

/*
 * int lista rendezése mergesorttal:
 * i) kettévágjuk a listát (egy ide, egy oda)
 * ii) külön-külön rendezzük a két részlistát
 * iii) összefésüljük őket (a kisebb elemet behúzva előbb a két rendezett listából)  
 */
object Mergesort extends App {
  /*
   * a visszatérési érték egy két listából álló PÁR
   * ezt így oda lehet adni egy val-nak akár val (left,right) = split(list) formában is
   */
  def split( list : List[ Int ] ) : (List[Int], List[Int]) = list match {
    case Nil => (Nil,Nil)
    case head :: Nil => (List(head),Nil)
    case first :: second :: tail => {
      val (list1, list2) = split( tail )
      (first :: list1, second :: list2 )
    }
  }
  
  /*
   * és párt lehet pattern matchelni is:
   */
  def merge( left : List[Int], right : List[Int] ) : List[Int] = (left,right) match {
    case (Nil,_) => right
    case (_,Nil) => left
    case ( a :: leftTail, b :: rightTail ) => if( a < b ) a :: merge( leftTail, right ) else b :: merge( left, rightTail )
  }
  
  def sort( list : List[Int] ) : List[Int] = list match {
    case Nil => Nil
    case _ :: Nil => list
    case _ => {
      // értékadni is lehet val-nak pár formátumban
      val (left,right) = split( list )
      merge( sort(left), sort(right ) )
    }
  }
  
  println( sort( List(1,4,2,8,5,7) ) ) // 1,2,4,5,7,8
}