package zh4

/**
 * Memó:
 * 1 until 10 -- visszaad egy Range objektumot, ami kb. a (1,2,...,9) lista az összes List-like metódussal
 * Stream.Empty -- az üres stream
 * head #:: tail -- ha tail : Stream[T], head : T, akkor beszúrja head-et az elejére. tail lazy!
 * Streamre is lehet matchelni így:
 * stream match {
 *   case Stream.Empty => ...
 *   case head #:: tail => ...
 * } 
 * stream1 #::: stream2 -- összefűzi a két streamet konstans időben
 * Option[T] : lehet None (nincs benne érték) vagy Some(v : T) (az érték v)
 */

/**
 * Ez egy (irányított, multi)gráf interface, do not touch
 * A template paraméter a node-ok típusa
 */
trait Graph[ T ] {
  /**
   * vertices : adjon vissza egy Stream-et a node-okról (így lehet végtelen gráfot is reprezentálni,
   * pl aminek minden Int egy-egy csúcsa)
   */
  def vertices : Stream[ T ]
  /**
   * getNeighboursOf( node : T ) -- adja vissza a node szomszédait a gráfban, streamként
   * (így lehet végtelen sok szomszédja is, ha számítva van a stream)
   */
  def getNeighboursOf( node : T ) : Stream[ T ]
}

/**
 * Implementáljunk egy a fenti trait-et extendelő gráfot, aminek
 * - a konstruktora kap egy n : Int-et
 * - a csúcsai 0..n-1 lesznek a számok
 * - az éleket éllistában tárolja: Array[ Stream[Int] ]-ben
 * - további metódusok: addEdge( from, to ) -- hozzáad egy élt; countEdges -- megszámolja az éleket
 * 
 * pl.
 * val graph = new GraphImpl(10)
 * for( (from,to) <- List( (0,1), (0,1), (1,2), (2,3) ) ) graph.addEdge(from,to)
 * graph.countEdges // returns 4 
 * 
 * További use case-ekhez lásd a test file-t
 */
class GraphImpl( val n : Int) extends Graph[Int] {
  // hint: sok minden közt van konverzió, pl toInt, toList, toStream
  private val nodes = 0 until n
  def vertices = nodes.toStream
  
  // hint: ahogy Javaban is, ilyenkor az Array null-okkal inicializálódik
  private val edges = new Array[ Stream[ Int ] ]( n )
  for( node <- nodes ) edges( node ) = Stream()
  
  def getNeighboursOf( node : Int ) = edges( node )
  
  // hint: vigyázat, lazy evaluation van
  def addEdge( from : Int, to : Int ) : Unit = edges( from ) = edges(from).+:(to) // wtf
  
  def countEdges : Int =  edges map {_.length} sum  
}

/**
 * A main task: szélességi keresés Graph[T]-ben Stream-ekkel
 */
object StreamZh {
  /**
   * Írj egy szélességi keresést! Input:
   * - graph    : a gráf
   * - from     : ebből a csúcsából induljon a keresés
   * - property : a from-hoz legközelebbi olyan csúcsot keresünk, melyre igaz property
   * returns:
   * None, ha nincs ilyen csúcs
   * Some( to ), ha to a from-hoz legközelebbi olyan csúcs, melyre igaz property.
   * 
   * Ha akarod, használd a lentebbi extend metódust hozzá. 
   */
  def search[T]( graph : Graph[T], from : T , property : T => Boolean ) : Option[T] = 
    extend( graph, Stream(from), Set(from), property )
  
  /**
   * Helper függvény, ha akarod, fejezd be és használd a search-hoz:
   * - graph : a gráf, amiben keresünk
   * - queue : a "szürke" csúcsok, amiknek a szomszédjait még nem terjesztettük ki
   * - seen  : a "látott" csúcsok, amik már benne vannak vagy voltak a queue-ban
   * - property : amilyen csúcsot keresünk
   * returns: Some(v), ha v a legelső megtalált csúcs, amire property igaz, None, ha nincs ilyen elérhető csúcs
   */
  def extend[T]( graph : Graph[T], queue : Stream[T], seen : Set[T], property : T => Boolean ) : Option[T] = queue match {
    case Stream.Empty => None
    case head #:: tail => if( property(head) ) Some(head) else {
      extend( graph, tail #::: graph.getNeighboursOf(head).filterNot( seen ), seen + head, property )
    }
  }
  
  /**
   * Írj egy metódust, ami egy fában a parent-eken lépkedve to-ból eljut from-ig!
   * Use case:
   * buildMap[Int]( Map(4->2, 3->2, 2->1, 1->0), 4, 0 ) should return Some(List(4,2,1,0))
   * mert 4 apja 2, 2 apja 1, 1 apja 0, tehát 4-nek 0 tényleg őse és az odavezető út (4,2,1,0).
   * Másik use case:
   * buildMap[Int]( Map(4->2, 3->2, 2->1, 1->0), 4, 3 ) should return None
   * mert 4 apja 2, 2 apja 1, 1 apja 0, 0-nak nincs apja, tehát 4-nek 3 nem őse.
   * 
   * Memó: a Map.get( k ) visszaad egy Option[T]-t, ami None, ha k nincs a map-ban kulcsként és Some(v),
   * ha a k-hoz rendelt érték v.
   * Az Option-ön is lehet matchelni, vagy mapni, vagy flatmapni is; utóbbiak None-ből None-t készítenek
   * Some(v)-ből Some(f(v))-t (map) vagy flatmapnél ha f(v)=Some(w), akkor Some(v)-ből Some(w) lesz,
   * ha f(v)=None, akkor Some(v)-ből None lesz.
   */  
  def buildMap[T]( parent : Map[T,T], to : T, from : T ) : Option[ List[T] ] =
    if( to == from ) Some( List( to ) ) else
      for( p <- parent.get(to) ; list <- buildMap( parent, p, from ) ) yield to :: list
  
}