package week02

/*
 * "a tail recursion pontosan ugyanarra jo, mint a while"
 */

import scala.annotation.tailrec

object While {
  // note: call-by-name, így a cond minden "rekurzív" hívásnál újra kiértékelődik
  @tailrec
  def apply( cond : =>Boolean )( proc : =>Unit ) : Unit = if( cond ) { proc; While(cond)(proc) }
  
  
  def main(args: Array[String]): Unit = {
    var n = 6 // just for testing purposes.. hogy a cond változhasson, kell állapot
    While( n > 0 ) { println( n ) ; n = n - 1 } // 6\n5\n4\n3\n2\n1
  }
}

object While2 {
  // closure, hogy ne kelljen dobálni a cond-ot és a proc-ot is
  def apply( cond : => Boolean )( proc : =>Unit ) : Unit = {
    @tailrec def recall : Unit = if( cond ) { proc ; recall }
    recall
  }
  
  def main(args: Array[String]): Unit = {
    var n = 6
    While2( n > 0 ) { println( n ); n = n - 1 } //mint fent
  }
}