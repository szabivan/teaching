package week02.beugro

import scala.annotation.tailrec

object Fibonacci {
  def apply( n : Int ) : Int = {
    @tailrec
    def fibAcc( n : Int, prev : Int, curr : Int ) : Int =
      if( n == 0 ) curr else fibAcc( n - 1, curr, prev+curr )
    if( n < 2 ) 1 else fibAcc( n, 0, 1 )
  }
  
  def main(args: Array[String]): Unit = {
    println( Fibonacci( 0 ) ) // 1
    println( Fibonacci( 1 ) ) // 1
    println( Fibonacci( 2 ) ) // 2
    println( Fibonacci( 3 ) ) // 3
    println( Fibonacci( 4 ) ) // 5
    println( Fibonacci( 5 ) ) // 8
  }
}