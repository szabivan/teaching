package week01

import scala.annotation.tailrec

object Main {
  
  def fact( n : Int ) : Int = if( n == 0 ) 1 else n * fact( n - 1 )
  
  def factTailrec( n : Int ) : Int = {
    // @tailrec annotáció: ha a függvény nem tail rekurzív, errort dob a fordító
    @tailrec
    def factHelper( n : Int , acc : Int ) : Int = if( n == 0 ) acc else factHelper( n - 1 , n * acc )
    factHelper( n , 1 )
  }
  
  /* call-by-value vs call-by-name paraméterátadás:
   * x-et előbb kiértékeljük, aztán hívjuk a függvényt, akinek mint értéket adjuk át (CBV)
   * y-t a kifejezést adjuk át, és csak akkor értékeljük ki, amikor (ha) az else ágra futunk
   * (call-by-name).
   * 
   * tehát call-by-value pontosan egyszer értékelődik ki,
   * call-by-name annyiszor, ahányszor hivatkozik rá az aktuális szál.
   * 
   * Ha van a kifejezésnek értéke, akkor az érték ugyanaz mindkét esetben.
   * A call-by-name akár exponenciálisan is lassabb lehet
   * Ha esetleg nincs érték (mert végtelen ciklus), akkor call-by-name terminál csak
   */
  def binaryOr( x : Boolean , y : =>Boolean ) = if(x) true else y
  
  // végtelen ciklus, erre binaryOr( true,loop ) =true, de ha CBV lenne y, akkor az is végtelen ciklus
  def loop : Boolean = loop
  
  def main(args: Array[String]): Unit = {
    println("Hello Scala")
    println( fact(5) ) // 120
    println( factTailrec( 5 ) ) // 120, kevesebb stackkel :P
  }
}