package week08.lazyval

import scala.util.Random

object StreamExample extends App {
  lazy val a = { println("NOW"); 7 }
  
  println("First")
  println("Second" + a)
  println("Third" + a )
  
  def infStream( from : Int ) : Stream[Int] = from #:: infStream( from + 1 )
  
  //built-in:
  def infStream2( from : Int ) = Stream.from( from )
  
  // continually: gets a CBN. Even it's random, it's stored
  val generator = Stream.continually( Random.nextInt(5) )
  
  println ( generator.take( 10 ) mkString(" ") )
  println ( generator.take( 20 ) mkString(" ") )
  
  //we can do fixed-point search
  def powerF( f : Double => Double )( start : Double ) : Stream[ Double ] = start #:: powerF( f )( f(start) )
  
  //a bit nicer with a closure
  def powerF2( f : Double => Double ) : Double => Stream[Double] = {
    def inner( start : Double ) : Stream[ Double ] = start #:: inner( f(start) )
    inner( _ )
  }
  
  def iterateTill( f : Double => Double )( cond : Double => Boolean )( start : Double ) : Stream[ Double ] =
    start #:: ( if( cond(start) ) Stream.Empty else iterateTill( f )( cond )( f(start) ) )
    
  def findFixedPoint( f : Double => Double )( prec : Double )( start : Double ) =
    iterateTill( f )( x => (Math.abs( x - f(x) ) < prec))(start)
    
  val fixedStream = findFixedPoint( x => 1-x/2 )( 0.0001 )( 0 )
  
  println( fixedStream mkString(" ") )
  
  val stream1 = Stream.continually( 1 )
  val stream2 = Stream.continually( 2 )
  
  val stream3 = stream1 #::: stream2
  
  println( stream3 take 10 mkString(" ") )
  
  def primesFrom( n : Int ) : Stream[Int] = n #:: ( primesFrom( n+1 ).filterNot( _%n != 0 ) )
  
  println( primesFrom(2) take 10 mkString "," )
}