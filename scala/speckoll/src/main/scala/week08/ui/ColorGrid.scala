package week08.ui

import scala.util.Random
import scalafx.beans.property.IntegerProperty
import scalafx.geometry.Point2D
import scala.collection.immutable.Vector
import scalafx.beans.property.BooleanProperty

// that's the Model
class ColorGrid( val height : Int , val width : Int, val n : Int ) {
  
  //notes: Array vs Vector vs List vs Tree  
  val data : Array[ Array[ IntegerProperty ] ] = new Array[ Array[IntegerProperty] ]( height )
  //notes: until -- Range. basically an iterator over 0..height-1. Int.until( end : Int )
  for( i <- 0 until height ) data(i) = new Array[IntegerProperty]( width )
  
  // inicializálás a játék kedvéért. Note: értékadás NEM apply, hanem update!!
  fillRandom( n )  
  def fillRandom( n : Int ) = for( i <- 0 until height; j <- 0 until width ) data(i)(j) = IntegerProperty( Random.nextInt( n ) )
  
  // nyerés teszt: ez nem Observable property, hanem hívni kell, mondjuk
  def isEveryone( c : Int ) = data.forall( _.forall( _.value==c ))
  def isHomogenous = isEveryone( data(0)(0).value )
  
  val directions = List( (1,0), (0,1), (-1,0), (0,-1) )
  
  def onMap( t : (Int,Int) ) = (t._1 >= 0) && (t._2 >= 0 ) && ( t._1 < height ) && (t._2 < width )
  
  def getNeighbours( t : (Int,Int) ) = directions.map { 
    that => (that._1+t._1,that._2+t._2)
  }. filter( onMap )
  
  //note: signalnak új értéket value_= ad
  def floodThere( from : Int , to : Int, coord : (Int,Int) ) : Unit = {
    println( "Flooding " + coord )
    if( this( coord ).value == from ) {
      //println( "flooded" )
      this( coord )() = to
      for( c <- getNeighbours( coord ) ) floodThere( from, to, c )
    }
  }
  
  // note: IntegerProperty aktuális értékét apply()-al kérjük el
  def floodIt( c : Int ) = {
    if( data(0)(0)() != c ) floodThere( data(0)(0).value, c, (0,0) )
    //println( "New top-left color is " + c ) 
  }
  
  // a ColorGrid-en (i)(j) visszaadja a megfelelő IntegerProperty-t
  def apply( i : Int )( j : Int ) = data(i)(j)
  def apply( coord : (Int,Int) ) : IntegerProperty = this.apply(coord._1)(coord._2)
  
}