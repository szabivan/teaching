package week08
import scalafx.scene.paint.Color

package object ui {
  val WINDOW_HEIGHT = 600
  val WINDOW_WIDTH  = 600
  val WINDOW_TITLE  = "Flood it!"
  val COLOR_MAP = Map[Int,Color]( 0 -> Color.Red , 1 -> Color.Aqua, 2 -> Color.Green, 3 -> Color.Yellow , 4 -> Color.Orange, 5 -> Color.Brown )
}