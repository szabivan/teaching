package week08.ui
//source: http://www.scalafx.org/docs/properties/

import scalafx.application.JFXApp

import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.paint.Color
import scalafx.scene.shape.Rectangle
import scalafx.Includes._ //ez kell a when-hez
import scalafx.scene.input.MouseEvent
import scalafx.beans.property.IntegerProperty
import scalafx.beans.property.ObjectProperty

import scalafx.beans.binding.ObjectBinding
import scalafx.beans.binding.Bindings
import scalafx.scene.Scene

// a JFXApp ad nekünk GUI-t
object FloodItMain extends JFXApp {
  // van neki egy modellje -- megkaphatná konstruktorban is, mindegy
  val grid = new ColorGrid( 10, 10, 5 )
  
  def createRectangle( i : Int , j : Int, nc : IntegerProperty, scene1: Scene ) = new Rectangle {
    width <== scene1.width / 10
    height <== scene1.height / 10
    layoutX <== width * j
    layoutY <== height * i
    
    def setFill = {
      val c = COLOR_MAP( nc() )
      val cd = c.deriveColor(0,1,0.90,1)
      fill <== when( hover ) choose {println("hover at " + i + "," + j ); c} otherwise cd
    }
    
    nc.onChange( setFill )
    setFill
    
    handleEvent( MouseEvent.MouseClicked ){
      (me : MouseEvent ) => floodTo( nc() )      
    }
  }
  
  def floodTo( i : Int ) = {
    grid.floodIt( i )
    if( grid.isHomogenous ) println("You won!")
  }
  
  // a stage varba kell rakjuk az ablakunkat
  stage = new PrimaryStage {
    //és felülírni a stage-ünk defaultjait -- a package objectből!
    title = WINDOW_TITLE
    width = WINDOW_WIDTH
    height = WINDOW_HEIGHT
    
    // a stage-be scenet pakolunk, hogy látsszon valami
    scene = new scalafx.scene.Scene {
      fill = Color.Navy  // a scene kitöltése      
      content = for( i <- 0 until 10 ; j <- 0 until 10) yield createRectangle( i, j, grid(i)(j), this )      
    }
    
  }
}