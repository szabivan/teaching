package week08

case class Coord( i : Int , j : Int ){
  import Coord._
  
  def +( that : Coord ) = Coord( this.i + that.i , this.j + that.j )
  
  def neighbours = directions map ( _ + this )
  def neighboursWith( p : Coord => Boolean ) = neighbours filter p  
}

object Coord {
  val UP   = Coord( 1, 0 )
  val DOWN = Coord(-1, 0 )
  val LEFT = Coord( 0,-1 )
  val RIGHT= Coord( 0, 1 )
  
  val directions = List( UP, DOWN, LEFT, RIGHT )
}