package week06.listOps

import scala.collection.immutable.Queue

object Primes extends App {
  def sieve( s : Stream[Int], till : Int ) : Stream[Int] = 
    s.head #:: (if( till > s.head*s.head ) sieve( s.tail filter { _%s.head != 0 }, till ) else s.tail ) 
  val primes = sieve( Stream.from(2), 16000000 )
  val t1 = System.nanoTime()
  println( primes.take(500000).last )
  val t2 = System.nanoTime()
  println( "Time elapsed " + (t2-t1)/1000000 + " ms")
  
  //val v = Vector( 1,4,2,8,5,7 )
  //for( i <- v.length-1 to 0 by -1 ) println( v(i) ) //kiírja visszafele
  //println( for( i <- v.length-1 to 0 by -1 ) yield v(i)*v(i) ) // Vector(49,25,64,4,16,1)  
}

