package week06.listOps

object Inverse extends PartialFunction[Double,Double] {
  def apply(x: Double) = 1.0 / x
  def isDefinedAt( x : Double ) = ( Math.abs( x ) > 0.0001 ) 
}

object Main extends App {  
  println( List(1,2,3,4,5,6,7).withFilter( x => { println(x);(x%2==0)} ).map( x=>{ println("x2");x*2} ) ) 
}