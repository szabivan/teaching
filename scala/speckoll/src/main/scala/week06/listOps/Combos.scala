package week06.listOps

object Combos {
  // map with fold
  def mapWithFold[T,U]( list : List[T], f : T=>U ) : List[U] =
    list.foldRight[List[U]](Nil)( f(_)::_  )
  
  // filter with fold
  def filterWithFold[T]( list : List[T], p : T => Boolean ) : List[T] =
    list.foldRight[List[T]](Nil)( (elem,tail) => if(p(elem)) elem :: tail else tail )
    
  // reduce with fold
  def reduceLeftWithFold[T,U >: T ]( list : List[T], op : (U,U)=>U ) = list match {
    case Nil => throw new UnsupportedOperationException( "Nil.reduce" )
    case head :: tail => tail.foldLeft[U](head)(op) 
  }
  
  // filter and map with a single fold 
  def filterMapWithFold[T, U]( list : List[T], p : T => Boolean, f : T => U ) : List[ U ] =
    list.foldRight[List[U]](Nil)( (elem,tail) => if( p(elem) ) f(elem)::tail else tail )
  
  // map with collect
  def mapWithCollect[T,U]( list : List[T], f : T=>U ) : List[U] = 
    list.collect{ case x => f(x) }
  
  // filter with collect
  def filterWithCollect[T]( list : List[T], p : T => Boolean ) : List[T] = 
    list.collect{ case x if p(x) => x }
  
  def invertElements( list : List[Double] ) : List[Double] =
    list.collect( { case x if( Math.abs(x) > 0.0001) => 1.0 / x } )
    
  def collectWithFlatMap[T,U]( list : List[T], f : PartialFunction[T,U] ) =
    list.flatMap( x => if( f.isDefinedAt(x) ) List(f(x)) else Nil ) 
  
  def flatMapWithFold[T,U]( list : List[T], f : T => List[U] ) : List[U] =
    list.foldRight[List[U]](Nil)( (elem,tail) => f(elem) ::: tail )
    
  def f( n : Int ) = n+1
  def g( n : Int ) = 2*n
    
  def chainFlatmaps( start : Int , map1 : Map[ Int, Int ], map2 : Map[ Int, Int ] , map3 : Map[ Int, Int ] ): Option[Int] ={
    Some(start) flatMap ( map1.get ) flatMap( map2.get ) flatMap( map3.get )
    // ha menet közben None lesz bármi, az eredmény is
  }
}