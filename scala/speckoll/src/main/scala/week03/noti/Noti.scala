package week03.noti

trait Noti 
/*
 *  konstruktor paraméter val : automatikusan létrehoz egy ilyen nevű gettert ehhez a mezőhöz
 *  var : gettert és settert ( pl sms.number = 7 )
 *  ezek nélkül: se getter, se setter
 *  private -al együtt : van getter (és var esetében setter), de csak az osztályon belülről érhető el
 */
class SMS( val number : Int ) extends Noti
class Email( val address : String ) extends Noti

object Handler {
  // match : mintaillesztés
  def handle( n : Noti ) = n match {
    // ez Javában kb. az "if (n instanceof SMS) { SMS sms = (SMS) n ; println(...) }" minta
    case sms : SMS => println("SMS jött innen: " + sms.number )
    case email : Email => println("Email jött innen: " + email.address )
    // a _ minta mindenre illeszkedik
    case _ => println("Unhandled Noti subtype! " + n.toString() )
  }
}