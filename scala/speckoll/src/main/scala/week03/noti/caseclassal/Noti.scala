package week03.noti.caseclassal

trait Noti
case class SMS( number : Int ) extends Noti
case class Email( address : String ) extends Noti

object Handler extends App {
  def handle( n : Noti ) = n match {
    case SMS( 666 ) => println("SMS jött a 666-ról" )
    case SMS( number ) => println("SMS jött innen: " + number )
    case Email( address ) => println("Email jött innen: " + address )
    case _ => println("Unhandled noti type: " + n )
  }
  
  val sms = SMS(666)
  val email = Email( "sanyi@kukac" )
  val sms2 = SMS(142857)
  
  handle( sms )  // SMS jött a 666-ról
  handle( email )// Email jött innen: sanyi@kukac
  handle( sms2 ) // SMS jött innen: 142857
}