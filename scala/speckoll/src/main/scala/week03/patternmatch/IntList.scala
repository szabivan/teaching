package week03.patternmatch

trait IntList {
  def ::( head : Int ) : IntList = Cons( head, this ) 
}

/*
 * A case object / class:
 * - konstruktor argumentumai val-ok (gettert generál hozzá)
 * - generál hozzá companion objectet factoryval => nem kell new, csak pl val a = Cons( 4, Nil )
 * - generál hozzá equalst / == -t és hashCode-t, az equals a mezők egyenlőségét nézi
 * - generál hozzá toString()-et "Cons(4,Nil)"
 * - lehet használni mintaillesztésben (ld lentebb)
 */
case object Nil extends IntList // az üres lista, nincs argumentum => elég belőle egy példány
// lehetne még case class Nil() extends IntList ehelyett
case class Cons( head : Int, tail : IntList ) extends IntList

case object Main extends App {
  
  /*
   *  mintaillesztés: egy objektum match { case p1 => e1 case p2 => e2 ... case pn => en } kifejezés
   *  hasonló a switch / case-hoz, de erősebb annál
   *  
   */
  
  def prettyPrint( list : IntList ) : String = list match {
    case Nil => ""
    case Cons( head, Nil ) => head.toString()
    case Cons( head, tail ) => head.toString() + "," + prettyPrint( tail )
  }
  
  // recall: a ':' végű operátor nevek BALRÓL tapadnak az objektumhoz, tehát ez a hívás lent
  // a Nil.::( 7 ).::( 5 ).::(8).::(2).::(4).::(1)
  val list = 1 :: 4 :: 2 :: 8 :: 5 :: 7 :: Nil
  
  println( prettyPrint( list ) ) // 1,4,2,8,5,7
}