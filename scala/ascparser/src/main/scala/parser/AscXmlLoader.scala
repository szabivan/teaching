package parser

import scala.xml.Elem
import java.io.PrintStream

case class Period  ( start: Int )
case class Day     ( name : String, id : Int )
case class Subject ( name : String )
case class Teacher ( name : String )
case class Room    ( name : String, capacity : Int )
case class Clazz   ( name : String )
case class Group   ( clazz : Clazz , name : String )
case class Lesson  ( clazzes : List[Clazz] , subject : Subject, length : Int, teachers : List[Teacher], rooms : List[Room], groups : List[Group] )
case class Card    ( lesson : Lesson, rooms : List[Room], period : Period, day : Day ) {
  
}

class AscXmlLoader( val xmlPath : String ) {
  val xml = scala.xml.XML.loadFile( xmlPath )
  
  val periods  = xml \\ "periods" \\ "period"   map { node => node.attribute("period").get.text -> Period( Integer.parseInt( node.attribute("short").get.text ) ) } toMap 
  val days     = xml \\ "daysdefs" \\ "daysdef" map { node => node.attribute("days").get.text -> Day( node.attribute("name").get.text, Integer.parseInt(node.attribute("days").get.text.split(",")(0)) )} toMap
  val subjects = xml \\ "subjects" \\ "subject" map { node => node.attribute("id").get.text -> Subject( node.attribute("name").get.text )} toMap
  val teachers = xml \\ "teachers" \\ "teacher" map { node => node.attribute("id").get.text -> Teacher( node.attribute("name").get.text )} toMap
  val rooms    = xml \\ "classrooms" \\ "classroom" map { node => node.attribute("id").get.text -> 
    Room( node.attribute("name").get.text,
    { val capacity = node.attribute("capacity").get.text
      if( capacity == "*" ) 0 else Integer.parseInt(capacity)
      } ) } toMap
  val clazzes = xml \\ "classes" \\ "class" map { node => node.attribute("id").get.text -> Clazz( node.attribute("name").get.text )} toMap
  val groups  = xml \\ "groups" \\ "group" map { node => node.attribute("id").get.text -> Group(
      clazzes( node.attribute("classid").get.text ),
      node.attribute("name").get.text
      )
      } toMap
  val lessons = xml \\ "lessons" \\ "lesson" map { node => node.attribute("id").get.text -> Lesson( 
    clazzes  = node.attribute("classids").get.text.split(",").filter( _.length > 0 ).map( clazzes ).toList,
    subject  = subjects( node.attribute("subjectid").get.text ),
    length   = Integer.parseInt( node.attribute("periodspercard").get.text ),
    teachers = node.attribute("teacherids").get.text.split(",").filter( _.length > 0 ).map( teachers ).toList,
    rooms    = node.attribute("classroomids").get.text.split(",").filter( _.length > 0 ).map( rooms ).toList,
    groups   = node.attribute("groupids").get.text.split(",").filter( _.length > 0 ).map( groups ).toList  
  )} toMap
  val cards = xml \\ "cards" \\ "card" map { node => Card(
      lesson  = lessons( node.attribute("lessonid").get.text ),
      rooms   = node.attribute("classroomids").get.text.split(",").filter( _.length > 0 ).map( rooms ).toList,
      period  = periods( node.attribute("period").get.text ),
      day     = days( node.attribute("days").get.text)
  )} toList
  val filteredCards = cards.filterNot( card => cards.exists(
    card2 => (card.lesson == card2.lesson)&&(card.day == card2.day) &&(card.period.start > card2.period.start)&&(card2.period.start + card.lesson.length > card.period.start )    
  ))  
  
  def cardSorter( c1 : Card, c2 : Card ) = (c1.day.id > c2.day.id) || (c1.day==c2.day && c1.period.start < c2.period.start ) 
  
  def collectCardsOf( teacher : Teacher ) = 
    filteredCards.filter( card => card.lesson.teachers.contains( teacher ) ).sortWith( cardSorter )
  
  def printByTeachers = {
    for( (teacherID, teacher) <- teachers.toSeq.sortBy( { case (id,teacher) => teacher.name } ) ){
      println("---------------------------------------------")
      println("Oktató: "+ teacher.name )
      for( card <- collectCardsOf( teacher ) ){
        println
        println( card.lesson.subject.name )
        println( card.lesson.clazzes.map( _.name ).mkString("   ", ", ", "") )
        println( "   Óraszám: " + card.lesson.length )
        println( "   Időpont: " + card.day.name + " " + card.period.start + " - " + (card.period.start + card.lesson.length ) )
        println( "   Terem: " + card.rooms.map( _.name ).mkString(", ") )
        if( card.lesson.teachers.length > 1 ) {
          println( "   Megjegyzés: társoktatók " + card.lesson.teachers.filterNot( t => t == teacher ).map( _.name ).mkString(", ") )
        }
      }
    }
  }
  def collectCardsOf( subject : Subject ) = filteredCards.filter( card => card.lesson.subject == subject ).sortWith( cardSorter )
  
  def printByCourses = {
    for( (subjectID, subject) <- subjects.toSeq.sortBy( { case (id, subject) => subject.name } ) ) {
      println("---------------------------------------------")
      println("Kurzus: "+ subject.name )
      for( card <- collectCardsOf( subject ) ){        
        println
        println( card.lesson.clazzes.map( _.name ).mkString("   ", ", ", "") )
        println( "   Oktató(k): " + card.lesson.teachers.map( _.name ).mkString(", ") )
        println( "   Óraszám: " + card.lesson.length )
        println( "   Időpont: " + card.day.name + " " + card.period.start + " - " + (card.period.start + card.lesson.length ) )
        println( "   Terem: " + card.rooms.map( _.name ).mkString(", ") )
      }
    }
  }  
  def collectCardsOf( room : Room ) = filteredCards.filter( card => card.rooms.contains( room ) ).sortWith( cardSorter )
  def printByRooms = {
    for( (roomID,room) <- rooms.toSeq.sortBy( { case (id,room) => room.name }) ){
      println("---------------------------------------------")
      println("Terem: "+ room.name + (if( room.capacity > 0 ) " (kapacitás: " + room.capacity + " fő)" else "" ) )
      for( card <- collectCardsOf( room ) ){
        println
        println( "   Kurzus: "+ card.lesson.subject.name )
        println( "   Oktató(k): " + card.lesson.teachers.map( _.name ).mkString(", ") )
        println( "   Időpont: " + card.day.name + " " + card.period.start + " - " + (card.period.start + card.lesson.length ) )
      }
    }
  }
}

object TestApp extends App {
  val loader = new AscXmlLoader( "nappalis.xml" )
  Console.setOut( new PrintStream("oktatok.txt", "8859_2") )
  loader.printByTeachers
  Console.setOut( new PrintStream("kurzusok.txt", "8859_2") )
  loader.printByCourses
  Console.setOut( new PrintStream("termek.txt", "8859_2") )
  loader.printByRooms
}