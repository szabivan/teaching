package data

class TimeSlot( val day : Int, val from : Int, val length : Int ) {
  def avoids( that : TimeSlot ) = (this.day != that.day ) || (this.from >= that.from + that.length ) || (that.from >= this.from + this.length )
  def hits( that : TimeSlot ) = !avoids(that)
}

object hetfo8 extends TimeSlot( 0, 8, 1 )
object hetfo9 extends TimeSlot( 0, 9, 1 )