package data

class Grid(
    var teachers    : List[ Teacher ],
    var courses     : List[ Course ],
    var cards       : List[ Card ],
    var constraints : List[ Constraint ]
  ){
  
  def cardsOf( teacher : Teacher ) = cards.filter( _.teachers.contains( teacher ))
  def cardsOf( place   : Terem   ) = cards.filter( _.place == place )
  def cardsOf( course  : Course  ) = cards.filter( _.courses.contains( course ) )

  
  
}