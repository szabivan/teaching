package data

trait TeremType
case object GEPTEREM extends TeremType
case object FILCES   extends TeremType
case object KRETAS   extends TeremType
//more to come

class Terem( val name : String, val shortName : String, val teremType : TeremType, val capacity : Int ) {
  
}