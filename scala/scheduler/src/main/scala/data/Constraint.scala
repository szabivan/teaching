package data

trait Constraint

case class Mandatory( course : Course , track : Track ) extends Constraint 