package data

trait TeacherStatus
case object STAFF     extends TeacherStatus
case object PHD       extends TeacherStatus
case object DEMO      extends TeacherStatus
case object EGYEB_OKT extends TeacherStatus

class Teacher(val firstName: String, val lastName: String, val shortName: String, val status : TeacherStatus ){  
}

object szabivan extends Teacher( "Szabolcs", "Iván", "szabivan", STAFF )
object kgelle   extends Teacher( "Kitti", "Gelle", "kgelle", PHD )