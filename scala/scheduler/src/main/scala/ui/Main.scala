package ui

import scalafx.application.JFXApp
import data.Grid
import scalafx.application.JFXApp.PrimaryStage
import scalafx.stage.Stage
import scalafx.scene.Scene
import scalafx.scene.paint.Color

object Main extends JFXApp {
  var grid : Grid = new Grid( Nil, Nil, Nil , Nil ) //todo
  
  stage = new PrimaryStage() {
    width  = WINDOW_WIDTH
    height = WINDOW_HEIGHT
    minWidth = WINDOW_MINWIDTH
    minHeight = WINDOW_MINHEIGHT
    title  = "Timetable clone"
    
    
    scene = new Scene() { //TODO ez menjen külön osztályba      
      def createTabview = {
        val tabView = new TabView()
        tabView.maxHeight <==> tabView.minHeight
        tabView.maxWidth  <==> tabView.minWidth
        tabView.maxHeight <== this.height - CARD_HEIGHT
        tabView.maxWidth  <== width
        tabView.layoutX   = 0
        tabView.layoutY   = 0
        tabView
      }
      def createCardpanel = {
        val cardPanel = new CardPanel()
        cardPanel.layoutY <== height - CARD_HEIGHT
        cardPanel
      }
      
      fill = Color.AntiqueWhite
      content.add( createTabview )
      content.add( createCardpanel )      
    }
  }
  
  
}