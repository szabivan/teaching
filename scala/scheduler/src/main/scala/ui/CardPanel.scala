package ui

import scalafx.scene.layout.StackPane
import scalafx.Includes._
import scalafx.scene.paint.Color
import javafx.scene.shape.Rectangle
import scalafx.scene.Node
import scalafx.scene.layout.Pane

class CardPanel extends Pane{
  minHeight = CARD_HEIGHT
  maxHeight = CARD_HEIGHT
  minWidth  = CARD_WIDTH
  maxWidth  = CARD_WIDTH
  layoutX   = 0
  layoutY   = 400
  
  children =
      new Rectangle(){        
        this.fill   <== when( hover ) choose Color.Cyan otherwise Color.Red
        this.width  = CARD_WIDTH
        this.height = CARD_HEIGHT
        this.layoutX = 0
        this.layoutY = 0
        this.arcHeight = 15
        this.arcWidth  = 15
      }
 
  
}