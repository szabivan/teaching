package ui.test

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafx.scene.paint.Color
import scalafx.scene.layout.GridPane
import scalafx.scene.layout.Pane
import scalafx.beans.property.DoubleProperty
import scalafx.scene.shape.Rectangle
import scalafx.Includes._

class ActiveRectangle(
    _paneWidth : Double,
    _paneHeight : Double,
    _flashWidth : Double,
    _flashHeight : Double ) extends Pane {
  
  def paneWidth = DoubleProperty( _paneWidth )
  def paneHeight = DoubleProperty( _paneHeight )
  def flashWidth = DoubleProperty( _flashWidth )
  def flashHeight = DoubleProperty( _flashHeight )
  
  val longRectangle = new Rectangle{ 
    width  <== paneWidth
    height <== paneHeight 
    fill    =  Color.Gray
  }
  val smallRectangle = new Rectangle{
    width <== flashWidth ;
    height <== flashHeight;
    fill <== when( longRectangle.hover ) choose Color.Yellow otherwise Color.Black
  }
  
  children.add( longRectangle )
  children.add( smallRectangle )
  
}

class TestScene extends Scene {
  fill = Color.Aqua  
  for( i <- 1 to 10 ){
    val col = new ActiveRectangle(50,550,50,50)
    col.layoutX = 50*i
    col.layoutY = 0
    content.add( col )
    
    val row = new ActiveRectangle(550,50,50,50)
    row.layoutX = 0
    row.layoutY = 50*i
    content.add( row )
  }
  
}

class TestStage extends PrimaryStage {
  height = 600
  width  = 600
  title  = "Test Window"
  scene  = new TestScene
}

object TestApp extends JFXApp {
  stage = new TestStage
}