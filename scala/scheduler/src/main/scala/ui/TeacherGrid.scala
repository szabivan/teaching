package ui

import scalafx.scene.layout.GridPane
import data.TimeSlot
import data.Teacher
import javafx.scene.control.Label
import data.hetfo8
import data.hetfo9
import data.szabivan
import data.kgelle
import scalafx.scene.layout.ColumnConstraints
import scalafx.scene.layout.Priority
import scalafx.scene.layout.RowConstraints
import scalafx.scene.shape.Rectangle
import scalafx.scene.paint.Color
import scalafx.Includes._
import scalafx.scene.input.MouseEvent
import scalafx.scene.Node
import scala.collection.mutable.ArrayBuffer

class TeacherGrid( val timeSlots : Vector[ TimeSlot ] , val teachers : Vector[ Teacher ] ) extends GridPane {
  val rowHeaders = ArrayBuffer[ Rectangle ]()
  val colHeaders = ArrayBuffer[ Rectangle ]()
  //val columnConstraint = new ColumnConstraints( MINICARD_WIDTH, MINICARD_WIDTH, Double.MaxValue )
  //columnConstraint.hgrow = Priority.Always
  
  //val rowConstraint = new RowConstraints( MINICARD_HEIGHT, MINICARD_HEIGHT, Double.MaxValue )
    
  for( i <- 0 until teachers.length ){
    
    val teacherRectangle = new Rectangle(){ width = MINICARD_WIDTH; height = MINICARD_HEIGHT; fill=Color.Aquamarine }
    rowHeaders.append( teacherRectangle )
    add( teacherRectangle, 0, i+1 )
    add( new Label( teachers(i).shortName ), 0, i+1 )
    //columnConstraints.add( columnConstraint )    
  }
  for( i <- 0 until timeSlots.length ) {
    val timeslotRectangle = new Rectangle(){ width = MINICARD_WIDTH; height = MINICARD_HEIGHT; fill=Color.Aquamarine }
    colHeaders.append( timeslotRectangle )
    add( timeslotRectangle, i+1, 0 )
    add( new Label( timeSlots(i).from.toString() ), i+1, 0 )
    //rowConstraints.add( rowConstraint )
  }
  for( i <- 1 to teachers.length ; j <- 1 to timeSlots.length ){
    add( new Rectangle(){
      height = MINICARD_HEIGHT
      width  = MINICARD_WIDTH
      fill   <== when( hover ) choose Color.ORANGE otherwise Color.Aquamarine
      
      handleEvent( MouseEvent.MouseEntered ) {
        ( me : MouseEvent ) => { rowHeaders(i-1).fill = Color.Yellow; colHeaders(j-1).fill = Color.Yellow }        
      }
      handleEvent( MouseEvent.MouseExited ) {
        ( me : MouseEvent ) => { rowHeaders(i-1).fill = Color.Aquamarine; colHeaders(j-1).fill = Color.Aquamarine }
      }
      
    } , j, i ) 
  }
}

object TestTeacherGrid extends TeacherGrid( Vector[TimeSlot]( hetfo8, hetfo9, hetfo9, hetfo9 ), Vector[Teacher]( szabivan, kgelle ) )