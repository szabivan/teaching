package ui



import scalafx.scene.control.TabPane
import scalafx.scene.control.Tab
import scalafx.scene.control.ScrollPane
import scalafx.scene.shape.Rectangle
import scalafx.scene.paint.Color
import scalafx.scene.control.ScrollPane.ScrollBarPolicy
import scalafx.scene.control.TabPane.TabClosingPolicy

class TabView extends TabPane {
  minWidth = 200
  maxWidth = 200
  minHeight = 200
  maxHeight = 200
  tabClosingPolicy = TabClosingPolicy.UNAVAILABLE
  tabs = Seq(
      new Tab(){
        text     = "Oktatók"
        content  = new ScrollPane() {
          hbarPolicy = ScrollBarPolicy.ALWAYS
          vbarPolicy = ScrollBarPolicy.ALWAYS
          fitToHeight = true
          fitToWidth  = true
          content = TestTeacherGrid
        }
      },
      new Tab(){
        text     = "Kurzusok"
      }
      )
}