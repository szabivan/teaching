

package object ui {
  // card mérete a gridben
  val MINICARD_HEIGHT = 50   // pontosan
  val MINICARD_WIDTH  = 100  // minimum
  
  // card mérete a bal alsó sarokban, fix
  val CARD_HEIGHT     = 150
  val CARD_WIDTH      = 150
  
  // ablak iniciális mérete
  val WINDOW_HEIGHT   = 600
  val WINDOW_WIDTH    = 600

  // ablak minimális mérete
  val WINDOW_MINHEIGHT   = 400
  val WINDOW_MINWIDTH    = 400

}